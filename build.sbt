name := "skunk"

version := "1.0"

scalaVersion := "2.13.4"

libraryDependencies ++= {

  Seq(
    "org.slf4j"             % "slf4j-api"               % "1.7.5",
    "ch.qos.logback"        % "logback-classic"         % "1.0.9",
    "org.tpolecat"          %% "skunk-core"             % "0.2.2",
  )

}