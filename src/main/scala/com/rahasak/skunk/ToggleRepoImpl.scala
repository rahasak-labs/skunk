package com.rahasak.skunk

import cats.effect.IO
import skunk.codec.all._
import skunk.implicits._
import skunk.{Session, _}

import java.time.LocalDateTime
import java.util.Date

/**
  * implement toggles repository functions
  * @param session skunk postgres session
  */
class ToggleRepoImpl(session: Session[IO]) extends ToggleRepo {

  /**
    * toggles timestamp filed in database stored as java.time.LocalDateTime,
    * toggles case class timestamp field defined as java.util.Data
    * so need function to convert java.util.Date into java.time.LocalDateTime when saving data
    * @param date util date
    * @return local date
    */
  def toLocalDateTime(date: Date) = {
    import java.time.{Instant, LocalDateTime, ZoneOffset}
    val instant = Instant.ofEpochMilli(date.getTime)
    val ldt = LocalDateTime.ofInstant(instant, ZoneOffset.UTC)
    ldt
  }

  /**
    * toggles timestamp filed in database stored as java.time.LocalDateTime,
    * toggles case class timestamp field defined as java.util.Data
    * so need function to convert java.time.LocalDateTime into java.util.Date when quering the data
    * @param ldt local date
    * @return util date
    */
  def toDate(ldt: LocalDateTime) = {
    import java.time.ZoneOffset
    val instant = ldt.toInstant(ZoneOffset.UTC)
    val date = Date.from(instant)
    date
  }

  /**
    * decode postgres data types into toggles case class
    * convert postgres timestamp into util.date
    */
  val toggleDecoder: Decoder[Toggle] =
    (uuid ~ varchar(50) ~ text ~ text ~ timestamp).map {
      case i ~ n ~ s ~ v ~ t => Toggle(Option(i), n, s, v, toDate(t))
    }

  /**
    * encode toggles case class data into postgres data types
    * convert toggle case class util.Date into postgres timestamp
    */
  val toggleEncoder = (varchar(50) ~ text ~ text ~ timestamp)
    .values.contramap((t: Toggle) => t.name ~ t.service ~ t.value ~ toLocalDateTime(t.timestamp))

  /**
    * create toggles
    * @param toggle toggle object
    * @return IO[Unit]
    */
  override def createToggle(toggle: Toggle): IO[Unit] = {
    // define query as skunk command
    // use toggle encoder to map toggle case class fields into postgres types
    val q: Command[Toggle] =
      sql"""
         INSERT INTO toggles(name, service, value, timestamp)
         VALUES $toggleEncoder
       """
        .command

    // execute command directly since nothing returned
    session.prepare(q).use(_.execute(toggle)).void
  }

  /**
    * update toggle with given name
    * @param name toggle name
    * @param value toggle value
    * @return IO[Unit]
    */
  override def updateToggle(name: String, value: String): IO[Unit] = {
    // define query as skunk command
    // query parameters passed as postgres types/codecs
    val q: Command[String ~ String] =
      sql"""
        UPDATE toggles
        SET value = $text
        WHERE name = $text
      """
        .command

    // execute command directly since nothing returned
    session.prepare(q).use(_.execute(value ~ name)).void
  }

  /**
    * delete toggle with given name
    * @param name toggle name
    * @return IO[Unit]
    */
  override def deleteToggle(name: String): IO[Unit] = {
    // define query as skunk command
    // query parameters passed as postgres types/codecs
    val q: Command[String] =
      sql"""
        DELETE FROM toggles
        WHERE name = $text
      """
        .command

    // execute command directly since nothing returned
    session.prepare(q).use(_.execute(name)).void
  }

  /**
    * get toggle with given name
    * @param name toggle name
    * @return IO[Toggle]
    */
  override def getToggle(name: String): IO[Toggle] = {
    val q: Query[String, Toggle] = {
      // define query as skunk query
      // used toggle decoder to decode postgres types into toggle case class
      sql"""
        SELECT id, name, service, value, timestamp
        FROM rahasak.toggles
        WHERE name = $text
        LIMIT 1
      """
        .query(toggleDecoder)
    }

    // create prepared statement with binding query parameters
    // execute query as fs2 stream
    // chunk size defines number of rows need to be fetched at once
    // getting a single row or throw error if not exists
    session.prepare(q).use { ps =>
      ps.stream(name, 64)
        //.take(1)
        .compile
        .lastOrError
    }
  }

  /**
    * get toggles with matching service name
    * @param service service name
    * @return IO[List[Toggles]]
    */
  override def getServiceToggles(service: String): IO[List[Toggle]] = {
    // define query as skunk query
    // used toggle decoder to decode postgres types into toggle case class
    val q: Query[String, Toggle] =
      sql"""
        SELECT id, name, service, value, timestamp
        FROM rahasak.toggles
        WHERE service = $text
        LIMIT 1
      """
        .query(toggleDecoder)

    // create prepared statement with binding query parameters
    // execute query as fs2 stream
    // chunk size defines number of rows need to be fetched at once
    // getting list rows
    session.prepare(q).use { ps =>
      ps.stream(service, 32)
        .compile
        .toList
    }
  }

  /**
    * get all toggles
    * @return IO[List[Toggles]]
    */
  override def getAllToggles: IO[List[Toggle]] = {
    // define query as skunk query
    // used toggle decoder to decode postgres types into toggle case class
    val q: Query[Void, Toggle] =
      sql"""
        SELECT id, name, service, value, timestamp
        FROM rahasak.toggles
      """
        .query(toggleDecoder)

    // execute query direcly with session
    session.execute(q)
  }
}
