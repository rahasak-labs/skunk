package com.rahasak.skunk

import cats.effect.IO

import java.util.{Date, UUID}

// define mapping to table toggles
case class Toggle(id: Option[UUID] = None, service: String, name: String, value: String, timestamp: Date = new Date())

/**
  * define curd operations of toggles table
  * return values wrapped with IO monad
  */
trait ToggleRepo {
  def createToggle(toggle: Toggle): IO[Unit]

  def updateToggle(name: String, value: String): IO[Unit]

  def deleteToggle(name: String): IO[Unit]

  def getToggle(name: String): IO[Toggle]

  def getServiceToggles(service: String): IO[List[Toggle]]

  def getAllToggles: IO[List[Toggle]]
}
