package com.rahasak.skunk

import cats.effect.kernel.Resource
import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import natchez.Trace.Implicits.noop
import skunk.Session

/**
  * IOApp which runs skunk queries
  */
object Main extends IOApp {

  // skunk session to postgres(in our case cockroachdb postgres api)
  // host defined the docker host
  // port is the docker container port
  val session: Resource[IO, Session[IO]] =
  Session.single(
    host = "192.168.64.25",
    port = 26257,
    user = "root",
    database = "rahasak",
    password = Option("root")
  )

  /**
    * IOApp run with IO monad unwrapping
    *
    * @param args args
    * @return IO[ExitCode]
    */
  def run(args: List[String]): IO[ExitCode] = {
    // unwrap session and execute various queries with IO monads
    session.use { s =>
      for {
        // implementation of skunk toggle repo with IO monad
        store <- IO(new ToggleRepoImpl(s))

        // create toggle, nothing returns on it
        _ <- store.createToggle(Toggle(name = "USE_STORAGE", service = "BASSA_SERVICE", value = "YES"))

        // update toggle, nothing returns on it
        _ <- store.updateToggle("USE_OPS", "NO")

        // get all toggles, as IO monad list
        allToggles <- store.getAllToggles
        _ <- IO.println(s"all toggles $allToggles")

        // get service toggles as IO monad list
        serviceToggles <- store.getServiceToggles("BASSA_SERVICE")
        _ <- IO.println(s"service toggles $serviceToggles")

        // get single toggle with IO monad
        toggle <- store.getToggle("USE_STORAGE")
        _ <- IO.println(s"toggle $toggle")

        // delete toggle, nothing return on it
        _ <- store.deleteToggle("USE_STORAGE")
      } yield ExitCode.Success
    }
  }

}
